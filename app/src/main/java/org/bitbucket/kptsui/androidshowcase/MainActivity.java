package org.bitbucket.kptsui.androidshowcase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = (ListView) findViewById(R.id.listView);
        String[] examples = getResources().getStringArray(R.array.examples);
        mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, examples));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        startActivity(new Intent(MainActivity.this, FileManagementActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(MainActivity.this, CameraActivity.class));
                        break;
                    default:
                        break;
                }
            }
        });
    }
}
