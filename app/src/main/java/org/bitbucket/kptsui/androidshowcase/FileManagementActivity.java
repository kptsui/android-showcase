package org.bitbucket.kptsui.androidshowcase;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileManagementActivity extends AppCompatActivity {

    /*
        The important part to save the image is to invoke:
        {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(fileUri);
            CameraActivity.this.sendBroadcast(mediaScanIntent);
        }
        Otherwise, the image file will not be save, although the uri can be used for a moment
     */

    public final static String TAG = "FileManagement Activity";
    public final static int REQUEST_SELECT_FILE = 0;

    ImageView mImageView;
    Bitmap mBitmap;
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_management);
        mHandler = new Handler();
        mImageView = (ImageView) findViewById(R.id.imageView);
    }

    public void btnSelectImage_Click(View v){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_SELECT_FILE);
    }

    public void btnSaveImage_Click(View v){
        if(mBitmap != null)
            saveImage(mBitmap);
        else{
            Toast.makeText(this, "mBitmap is null! Failed to save the image.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "mBitmap is null! Failed to save the image.");
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if( resultCode != RESULT_OK)
            return;

        if (requestCode == REQUEST_SELECT_FILE){
            try {
                mBitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                mImageView.setImageBitmap(mBitmap);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void saveImage(final Bitmap bitmap){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/Showcase";
                File folder = new File(dir);
                folder.mkdirs();

                String fileName = "Showcase_" + System.currentTimeMillis() + ".jpg";
                File file = new File(folder, fileName);
                if (file.exists()) file.delete();

                Log.i(TAG, "saving file: " + file.toString());

                try {
                    FileOutputStream out = new FileOutputStream(file);
                    //Bitmap bm = bitmapCache.copy(bitmapCache.getConfig(), true);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, out);
                    Log.i(TAG, "bitmap compress to FileOutputStream");

                    out.flush();
                    out.close();
                    Log.i(TAG, "FileOutputStream close");

                    // saving the image file
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(Uri.fromFile(file));
                    FileManagementActivity.this.sendBroadcast(mediaScanIntent);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Save image failed");
                }
            }
        }).start();
    }

}
